// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CharacterController.generated.h"

UCLASS()
class FIRSTPROJECT_API ACharacterController : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACharacterController();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//Character movement func
	void MoveForward(float Value);
	void TurnAtRate(float Value);
	void LookUpAtRate(float Value);
	void MoveRight(float Value);

	//Character movement args
	UPROPERTY(EditAnywhere, Category = "Player Camera")
	float BaseTurnRate;
	UPROPERTY(EditAnywhere, Category = "Player Camera")
	float BaseLookUpRate; 

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind the input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override; 


};
